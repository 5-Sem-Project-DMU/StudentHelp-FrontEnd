package com.studenthelp.web;

import com.studenthelp.web.controllers.HomeController;
import com.studenthelp.web.controllers.OpportunityController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication

public class Application {

    public static final String JOB_URL = "HTTP://JOBLISTING";
    public static void main(String[] args) {
        System.setProperty("spring.config.name","webserver");
        SpringApplication.run(Application.class);
    }

    @Bean
    public HomeController homeController(){
        return new HomeController();
    }

    @Bean
    public OpportunityController opportunityController(){
        return new OpportunityController();
    }

}
